package com.univalle.jwt.infraestructura.controlador;

import com.univalle.jwt.aplicacion.servicio.RolServicio;
import com.univalle.jwt.dominio.modelo.dto.UsuarioLogin;
import com.univalle.jwt.aplicacion.servicio.UsuarioServicio;
import com.univalle.jwt.dominio.modelo.dto.JwtDTO;
import com.univalle.jwt.dominio.modelo.dto.UsuarioRegistro;
import com.univalle.jwt.dominio.modelo.entidad.Rol;
import com.univalle.jwt.dominio.modelo.entidad.Usuario;
import com.univalle.jwt.dominio.modelo.enums.RolNombre;
import com.univalle.jwt.infraestructura.seguridad.jwt.JwtProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.HashSet;
import java.util.Set;

@RestController
@RequestMapping("/api/v1/auth")
public class AuthController {

    @Autowired
    PasswordEncoder passwordEncoder;

    @Autowired
    AuthenticationManager authenticationManager;

    @Autowired
    UsuarioServicio usuarioService;

    @Autowired
    RolServicio rolService;

    @Autowired
    JwtProvider jwtProvider;

    @PostMapping("/registro")
    public ResponseEntity<?> nuevo(@Valid @RequestBody UsuarioRegistro nuevoUsuario, BindingResult bindingResult){
        if(bindingResult.hasErrors())
            return new ResponseEntity("{\"mensaje\" : \"Campos vacíos o email inválido\"}", HttpStatus.BAD_REQUEST);
        if(usuarioService.existePorNombre(nuevoUsuario.getNombreUsuario()))
            return new ResponseEntity("{\"mensaje\" : \"Nombre de usuario ya existe\"}", HttpStatus.BAD_REQUEST);
        if(usuarioService.existePorEmail(nuevoUsuario.getEmail()))
            return new ResponseEntity("{\"mensaje\" : \"Email ya existe\"}", HttpStatus.BAD_REQUEST);
        Usuario usuario =
                new Usuario(nuevoUsuario.getNombre(), nuevoUsuario.getNombreUsuario(), nuevoUsuario.getEmail(),
                        passwordEncoder.encode(nuevoUsuario.getPassword()));
        Set<String> rolesStr = nuevoUsuario.getRoles();
        Set<Rol> roles = new HashSet<>();
        for (String rol : rolesStr) {
            switch (rol) {
                case "admin":
                    Rol rolAdmin = rolService.getByRolNombre(RolNombre.ROLE_ADMINISTRADOR).get();
                    roles.add(rolAdmin);
                    break;
                case "invitado":
                    Rol rolInvitado = rolService.getByRolNombre(RolNombre.ROLE_INVITADO).get();
                    roles.add(rolInvitado);
                    break;
                default:
                    Rol rolUser = rolService.getByRolNombre(RolNombre.ROLE_MESERO).get();
                    roles.add(rolUser);
            }
        }
        usuario.setRoles(roles);
        usuarioService.guardar(usuario);
        return new ResponseEntity("{\"mensaje\" : \"Usuario creado satisfactoriamente\"}", HttpStatus.CREATED);
    }

    @PostMapping("/login")
    public ResponseEntity<JwtDTO> login(@Valid @RequestBody UsuarioLogin loginUsuario, BindingResult bindingResult){
        if(bindingResult.hasErrors())
            return new ResponseEntity("{\"mensaje\" : \"Campos vacíos o email inválido\"}", HttpStatus.BAD_REQUEST);
        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(loginUsuario.getNombreUsuario(), loginUsuario.getPassword())
        );
        SecurityContextHolder.getContext().setAuthentication(authentication);
        String jwt = jwtProvider.generateToken(authentication);
        UserDetails userDetails = (UserDetails) authentication.getPrincipal();
        JwtDTO jwtDTO = new JwtDTO(jwt, userDetails.getUsername(), userDetails.getAuthorities());
        return new ResponseEntity<JwtDTO>(jwtDTO, HttpStatus.OK);
    }

    @GetMapping("/usuario")
    public ResponseEntity<Usuario> getUsuario(){
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if(!(authentication instanceof AnonymousAuthenticationToken)){
            Usuario usuario = usuarioService.getByNombreUsuario(authentication.getName()).get();
            return new ResponseEntity<Usuario>(usuario, HttpStatus.OK);
        }else{
            return new ResponseEntity("{\"mensaje\" : \"No se reconoce el usuario autenticado\"}", HttpStatus.NOT_FOUND);
        }
    }

}
