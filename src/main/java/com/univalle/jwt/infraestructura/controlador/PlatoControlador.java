package com.univalle.jwt.infraestructura.controlador;

import com.univalle.jwt.aplicacion.servicio.PlatoServicio;
import com.univalle.jwt.dominio.modelo.entidad.Plato;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/platos")
public class PlatoControlador {

    @Autowired
    PlatoServicio platoServicio;

    @GetMapping()
    @PreAuthorize("hasRole('INVITADO')"+"hasRole('MESERO')"+"hasRole('ADMINISTRADOR')")
    public ResponseEntity<List<Plato>> getLista(){
        List<Plato> lista = platoServicio.obtenerTodos();
        return new ResponseEntity<List<Plato>>(lista, HttpStatus.OK);
    }

    @GetMapping("/{id}")
    @PreAuthorize("hasRole('MESERO')"+"hasRole('ADMINISTRADOR')")
    public ResponseEntity<Plato> getOne(@PathVariable Long id){
        if(!platoServicio.existePorId(id))
            return new ResponseEntity("{\"mensaje\" : \"No existe el plato consultado\"}", HttpStatus.NOT_FOUND);
        Plato plato = platoServicio.obtenerPorId(id).get();
        return new ResponseEntity<Plato>(plato, HttpStatus.OK);
    }


    @PostMapping()
    @PreAuthorize("hasRole('ADMINISTRADOR')" +"hasRole('MESERO')")
    public ResponseEntity<?> create(@RequestBody Plato plato){
        if(StringUtils.isBlank(plato.getNombrePlato()))
            return new ResponseEntity("{\"mensaje\" : \"El nombre es obligatorio\"}", HttpStatus.BAD_REQUEST);
        if(plato.getValor() == null || plato.getValor()==0)
            return new ResponseEntity("{\"mensaje\" : \"El precio es obligatorio\"}", HttpStatus.BAD_REQUEST);
        if(platoServicio.existePorNombre(plato.getNombrePlato()))
            return new ResponseEntity("{\"mensaje\" : \"El nombre del plato ya existe\"}", HttpStatus.BAD_REQUEST);
        if(plato.isActivo())
            return new ResponseEntity("{\"mensaje\" : \"El plato no tiene un activo asignado\"}", HttpStatus.BAD_REQUEST);
        if(plato.getFechaCreacion()== null)
            return new ResponseEntity("{\"mensaje\" : \"El plato no tiene una fecha asignada\"}", HttpStatus.BAD_REQUEST);
        platoServicio.guardar(plato);
        return new ResponseEntity("{\"mensaje\" : \"Plato Creado\"}", HttpStatus.CREATED);
    }

    @PutMapping("/{id}")
    public ResponseEntity<?> update(@RequestBody Plato plato, @PathVariable("id") Long id){
        if(!platoServicio.existePorId(id))
            return new ResponseEntity("{\"mensaje\" : \"No existe el plato\"}", HttpStatus.NOT_FOUND);
        if(StringUtils.isBlank(plato.getNombrePlato()))
            return new ResponseEntity("{\"mensaje\" : \"El nombre es obligatorio\"}", HttpStatus.BAD_REQUEST);
        if(plato.getValor() == null || plato.getValor()==0)
            return new ResponseEntity("{\"mensaje\" : \"El precio es obligatorio\"}", HttpStatus.BAD_REQUEST);
        if(platoServicio.existePorNombre(plato.getNombrePlato()) &&
                platoServicio.obtenerPorNombre(plato.getNombrePlato()).get().getId() != id)
            return new ResponseEntity("{\"mensaje\" : \"El nombre ya existe\"}", HttpStatus.BAD_REQUEST);
        Plato platoUpdate = platoServicio.obtenerPorId(id).get();
        platoUpdate.setNombrePlato(plato.getNombrePlato());
        platoUpdate.setDescripcion(plato.getDescripcion());
        platoUpdate.setValor(plato.getValor());
        platoUpdate.setActivo(plato.isActivo());
        platoUpdate.setFechaCreacion(plato.getFechaCreacion());
        platoServicio.guardar(platoUpdate);
        return new ResponseEntity("{\"mensaje\" : \"Producto actualizado\"}", HttpStatus.CREATED);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> delete(@PathVariable Long id){
        if(!platoServicio.existePorId(id))
            return new ResponseEntity("{\"mensaje\" : \"No existe ese producto\"}", HttpStatus.NOT_FOUND);
        platoServicio.eliminar(id);
        return new ResponseEntity("{\"mensaje\" : \"Producto eliminado\"}", HttpStatus.OK);
    }

}
