package com.univalle.jwt.infraestructura.repositorio;

import com.univalle.jwt.aplicacion.repositorio.PlatoRepositorio;
import com.univalle.jwt.dominio.modelo.entidad.Plato;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface PlatoRepositorioJPA extends PlatoRepositorio {

    @Override
    public Optional<Plato> findByNombrePlato(String nombrePlato);

    @Override
    public boolean existsByNombrePlato(String nombrePlato);
}
