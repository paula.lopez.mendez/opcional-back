package com.univalle.jwt.infraestructura.repositorio;

import com.univalle.jwt.aplicacion.repositorio.RolRepositorio;
import com.univalle.jwt.dominio.modelo.entidad.Rol;
import com.univalle.jwt.dominio.modelo.enums.RolNombre;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface RolRepositorioJPA extends JpaRepository<Rol, Long> {

    Optional<Rol> findByRolNombre(RolNombre rolNombre);
}
