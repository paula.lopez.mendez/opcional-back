package com.univalle.jwt.infraestructura.repositorio;

import com.univalle.jwt.aplicacion.repositorio.UsuarioRepositorio;
import com.univalle.jwt.dominio.modelo.entidad.Usuario;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UsuarioRepositorioJPA extends JpaRepository<Usuario, Long> {

    Optional<Usuario> findByNombreUsuario(String nombreUsuario);


    boolean existsByNombreUsuario(String nombreUsuario);


    boolean existsByEmail(String email);
}
