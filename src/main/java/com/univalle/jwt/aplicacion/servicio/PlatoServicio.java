package com.univalle.jwt.aplicacion.servicio;

import com.univalle.jwt.dominio.modelo.entidad.Plato;

import java.util.List;
import java.util.Optional;

public interface PlatoServicio {

     List<Plato> obtenerTodos();

     Optional<Plato> obtenerPorId(Long id);

     Optional<Plato> obtenerPorNombre(String nombrePlato);

     void guardar(Plato plato);

     void eliminar(Long id);

     boolean existePorNombre(String nombrePlato);

     boolean existePorId(Long id);
}
