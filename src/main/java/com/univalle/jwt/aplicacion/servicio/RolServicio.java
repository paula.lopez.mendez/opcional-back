package com.univalle.jwt.aplicacion.servicio;

import com.univalle.jwt.dominio.modelo.entidad.Rol;
import com.univalle.jwt.dominio.modelo.enums.RolNombre;

import java.util.Optional;

public interface RolServicio {

    Optional<Rol> getByRolNombre(RolNombre rolNombre);
}
