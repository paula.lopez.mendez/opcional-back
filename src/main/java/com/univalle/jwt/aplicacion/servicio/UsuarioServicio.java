package com.univalle.jwt.aplicacion.servicio;

import com.univalle.jwt.dominio.modelo.entidad.Usuario;

import java.util.Optional;

public interface UsuarioServicio {

    Optional<Usuario> getByNombreUsuario(String nombreUsuario);

    boolean existePorNombre(String nombreUsuario);

    boolean existePorEmail(String email);

    void guardar(Usuario usuario);

}
