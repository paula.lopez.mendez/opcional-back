package com.univalle.jwt.aplicacion.repositorio;

import com.univalle.jwt.dominio.modelo.entidad.Plato;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface PlatoRepositorio extends JpaRepository<Plato, Long> {

    Optional<Plato> findByNombrePlato(String np);
    boolean existsByNombrePlato(String np);
}
