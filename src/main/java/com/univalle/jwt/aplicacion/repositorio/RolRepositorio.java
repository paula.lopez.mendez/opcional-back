package com.univalle.jwt.aplicacion.repositorio;

import com.univalle.jwt.dominio.modelo.entidad.Rol;
import com.univalle.jwt.dominio.modelo.enums.RolNombre;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface RolRepositorio extends JpaRepository<Rol, Long> {
    Optional<Rol> findByRolNombre(RolNombre rolNombre);
}
