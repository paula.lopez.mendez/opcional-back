package com.univalle.jwt.dominio.modelo.enums;

public enum RolNombre {
    ROLE_ADMINISTRADOR,
    ROLE_INVITADO,
    ROLE_MESERO
}
