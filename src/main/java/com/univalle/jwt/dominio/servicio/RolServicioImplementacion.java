package com.univalle.jwt.dominio.servicio;

import com.univalle.jwt.aplicacion.repositorio.RolRepositorio;
import com.univalle.jwt.aplicacion.servicio.RolServicio;
import com.univalle.jwt.dominio.modelo.entidad.Rol;
import com.univalle.jwt.dominio.modelo.enums.RolNombre;
import com.univalle.jwt.infraestructura.repositorio.RolRepositorioJPA;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Service
@Transactional
public class RolServicioImplementacion implements RolServicio {

    @Autowired
    RolRepositorioJPA rolRepository;

    @Override
    public Optional<Rol> getByRolNombre(RolNombre rolNombre){
        return rolRepository.findByRolNombre(rolNombre);
    }
}
