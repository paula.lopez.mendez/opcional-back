package com.univalle.jwt.dominio.servicio;

import com.univalle.jwt.aplicacion.repositorio.UsuarioRepositorio;
import com.univalle.jwt.aplicacion.servicio.UsuarioServicio;
import com.univalle.jwt.dominio.modelo.entidad.Usuario;
import com.univalle.jwt.infraestructura.repositorio.UsuarioRepositorioJPA;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Service
@Transactional
public class UsuarioServicioImplementacion implements UsuarioServicio {

    @Autowired
    UsuarioRepositorioJPA usuarioRepository;

    @Override
    public Optional<Usuario> getByNombreUsuario(String nombreUsuario){
        return usuarioRepository.findByNombreUsuario(nombreUsuario);
    }

    @Override
    public boolean existePorNombre(String nombreUsuario){
        return usuarioRepository.existsByNombreUsuario(nombreUsuario);
    }

    @Override
    public  boolean existePorEmail(String email){
        return usuarioRepository.existsByEmail(email);
    }

    @Override
    public void guardar(Usuario usuario){
        usuarioRepository.save(usuario);
    }
}
