package com.univalle.jwt.dominio.servicio;

import com.univalle.jwt.aplicacion.repositorio.PlatoRepositorio;
import com.univalle.jwt.aplicacion.servicio.PlatoServicio;
import com.univalle.jwt.dominio.modelo.entidad.Plato;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class PlatoServicioimplementacion implements PlatoServicio {

    @Autowired
    PlatoRepositorio platoRepositorio;


    @Override
    public List<Plato> obtenerTodos() {
        List<Plato> lista = platoRepositorio.findAll();
        return lista;
    }

    @Override
    public Optional<Plato> obtenerPorId(Long id) {
        return platoRepositorio.findById(id);
    }

    @Override
    public Optional<Plato> obtenerPorNombre(String nombrePlato) {
        return platoRepositorio.findByNombrePlato(nombrePlato);
    }

    @Override
    public void guardar(Plato plato) {
        platoRepositorio.save(plato);
    }

    @Override
    public void eliminar(Long id) {
        platoRepositorio.deleteById(id);
    }

    @Override
    public boolean existePorNombre(String nombrePlato) {
        return platoRepositorio.existsByNombrePlato(nombrePlato);
    }

    @Override
    public boolean existePorId(Long id) {
        return platoRepositorio.existsById(id);
    }
}
